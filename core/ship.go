package core

type ShipType int

type ShipSize int

const (
	CarrierSize    ShipSize = 5
	BattleshipSize ShipSize = 4
	CruiserSize    ShipSize = 3
	SubmarineSize  ShipSize = 3
	DestroyerSize  ShipSize = 2
)

const (
	Carrier ShipType = iota
	Battleship
	Cruiser
	Submarine
	Destroyer
)

var shipTypesByName = map[string]ShipType{
	"carrier":    Carrier,
	"battleship": Battleship,
	"cruiser":    Cruiser,
	"submarine":  Submarine,
	"destroyer":  Destroyer,
}

var shipSizesByName = map[string]ShipSize{
	"carrier":    CarrierSize,
	"battleship": BattleshipSize,
	"cruiser":    CruiserSize,
	"submarine":  SubmarineSize,
	"destroyer":  DestroyerSize,
}

var shipSizesByType = map[ShipType]ShipSize{
	Carrier:    CarrierSize,
	Battleship: BattleshipSize,
	Cruiser:    CruiserSize,
	Submarine:  SubmarineSize,
	Destroyer:  DestroyerSize,
}

type Ship struct {
	shipType ShipType
	size     ShipSize
	hits     int
}
