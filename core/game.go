package core

var maxGameID int = 0

type gameState int

type Game struct {
	id         int
	player1    *Player
	player2    *Player
	playerTurn *Player
	history    []string
}

type GameHistory struct {
	action    string
	timestamp string
}

func NewGame(player1 *Player, player2 *Player) *Game {
	maxGameID++
	return &Game{maxGameID, player1, player2, player1, []string{}}
}
