package core

import "fmt"

type actionType int

const (
	newPlayer actionType = iota
	processMessage
	lookingForGame
	foundGame
	cancelLfg
	joinGame
	startGame
	placeShips
	turn
	waitForTurn
	victory
	concede
	serverError
)

type Action struct {
	UserName   string     `json:"userName"`
	UserId     int        `json:"userId"`
	ActionType actionType `json:"actionType"`
	Body       string     `json:"body"`
	Timestamp  string     `json:"timestamp"`
}

func NewAction(username string, userId int, action actionType, body string, timestamp string) *Action {
	return &Action{username, userId, action, body, timestamp}
}

func (self *Action) String() string {
	return self.UserName + " at " + self.Timestamp + " with actionType " + fmt.Sprint(self.ActionType) + " and body " + self.Body
}
