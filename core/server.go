package core

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Server struct {
	connectedPlayers  map[int]*Player
	Messages          []*Action `json: messages`
	Games             map[int]*Game
	addPlayer         chan *Player
	removePlayer      chan *Player
	newIncomingAction chan *Action
	errorChannel      chan error
	doneCh            chan bool
}

func NewServer() *Server {
	Messages := []*Action{}
	Games := make(map[int]*Game)
	connectedPlayers := make(map[int]*Player)
	addPlayer := make(chan *Player)
	removePlayer := make(chan *Player)
	newIncomingAction := make(chan *Action)
	errorChannel := make(chan error)
	doneCh := make(chan bool)
	return &Server{
		connectedPlayers,
		Messages,
		Games,
		addPlayer,
		removePlayer,
		newIncomingAction,
		errorChannel,
		doneCh,
	}
}

func (server *Server) AddPlayer(player *Player) {
	log.Println("In AddPlayer")
	server.addPlayer <- player
}

func (server *Server) RemovePlayer(player *Player) {
	log.Println("Removing player")
	server.removePlayer <- player
}

func (server *Server) ProcessNewAction(action *Action) {
	log.Println("In ProcessNewAction ", action)
	server.newIncomingAction <- action
}

func (server *Server) Done() {
	server.doneCh <- true
}

func (server *Server) sendPastMessages(player *Player) {
	for _, msg := range server.Messages {
		//  log.Println("In sendPastMessages writing ",msg)
		player.Write(msg)
	}
}

func (server *Server) sendPlayerAction(player *Player, action *Action) {
	player.Write(action)
}

func (server *Server) Err(err error) {
	server.errorChannel <- err
}

func (server *Server) sendAll(msg *Action) {
	log.Println("In Sending to all Connected players")
	for _, player := range server.connectedPlayers {
		player.Write(msg)
	}
}

func (server *Server) Listen() {
	log.Println("Server Listening .....")

	http.HandleFunc("/battleship", server.handleActions)
	http.HandleFunc("/getAllMessages", server.handleGetAllMessages)

	for {
		select {
		// Adding a new player
		case player := <-server.addPlayer:
			log.Println("Added a new Player")
			server.connectedPlayers[player.id] = player
			log.Println("Now ", len(server.connectedPlayers), " players are connected to server")
			var actionObject Action = Action{player.userName, player.id, newPlayer, "", ""}

			server.sendPlayerAction(player, &actionObject)

			server.sendPastMessages(player)

		case player := <-server.removePlayer:
			log.Println("Removing player from server")
			var actionObject Action
			for _, game := range server.Games {
				if game.player1 == player {
					game.player2.state = available
					actionObject = Action{game.player2.userName, game.player2.id, cancelLfg, "", ""}
					game.player2.Write(&actionObject)
					delete(server.Games, game.id)
					delete(server.connectedPlayers, player.id)
				} else if game.player2 == player {
					actionObject = Action{game.player1.userName, game.player1.id, cancelLfg, "", ""}
					game.player1.state = available
					game.player1.Write(&actionObject)
					delete(server.Games, game.id)
					delete(server.connectedPlayers, player.id)
				}
			}

		case action := <-server.newIncomingAction:
			println("newIncomingMessage")
			if action.ActionType == processMessage {
				server.Messages = append(server.Messages, action)
				server.sendAll(action)
			} else if action.ActionType == lookingForGame {
				print("Looking for game")
				var player = server.connectedPlayers[action.UserId]
				player.state = lfgQueue
				for _, lfgPlayer := range server.connectedPlayers {
					if lfgPlayer != player && lfgPlayer.state == lfgQueue {
						print("Found player for LFG")
						player.state = waitingForgame
						lfgPlayer.state = waitingForgame
						actionObject := Action{player.userName, player.id, foundGame, "", ""}
						player.Write(&actionObject)
						actionObject2 := Action{lfgPlayer.userName, lfgPlayer.id, foundGame, "", ""}
						lfgPlayer.Write(&actionObject2)
						game := NewGame(player, lfgPlayer)
						server.Games[game.id] = game
					}
				}
			} else if action.ActionType == cancelLfg {
				print("Player canceling lfg")
				var player = server.connectedPlayers[action.UserId]
				if player.state == waitingForgame {
					var actionObject Action
					for _, game := range server.Games {
						if game.player1 == player {
							game.player2.state = available
							actionObject = Action{game.player2.userName, game.player2.id, cancelLfg, "", ""}
							game.player2.Write(&actionObject)
							delete(server.Games, game.id)
							player.state = available
						} else if game.player2 == player {
							actionObject = Action{game.player1.userName, game.player1.id, cancelLfg, "", ""}
							game.player1.state = available
							game.player1.Write(&actionObject)
							delete(server.Games, game.id)
							player.state = available
						}
					}
				}
			} else if action.ActionType == joinGame {
				print("Player joined game")
				var player = server.connectedPlayers[action.UserId]
				if player.state == waitingForgame {
					player.state = readyForGame
					for _, game := range server.Games {
						if game.player1 == player {
							if game.player2.state == readyForGame {
								actionObject := Action{player.userName, player.id, startGame, "", ""}
								player.Write(&actionObject)
								actionObject2 := Action{game.player2.userName, game.player2.id, startGame, "", ""}
								game.player2.Write(&actionObject2)
							}
						} else if game.player2 == player {
							if game.player1.state == readyForGame {
								actionObject := Action{player.userName, player.id, startGame, "", ""}
								player.Write(&actionObject)
								actionObject2 := Action{game.player1.userName, game.player1.id, startGame, "", ""}
								game.player1.Write(&actionObject2)
							}

						}
					}
				}
			} else if action.ActionType == placeShips {
				println("Player placing ships")
				var player = server.connectedPlayers[action.UserId]
				var ships map[string]interface{}
				err := json.Unmarshal([]byte(action.Body), &ships)
				if err != nil {

				}

				for key, value := range ships {
					coordinatesArr := strings.Split(fmt.Sprintf("%v", value), ",")
					fmt.Println(key)
					shipSize := shipSizesByName[key]
					shipType := shipTypesByName[key]
					coordinatesArr = sortCoordinates(coordinatesArr)
					isInOrder := isCoordinatesInOrder(coordinatesArr)
					if !isInOrder {
						errorBody := `{
							"error": "` + key + ` is not in a linear sequential order ex A1,A2... A1,B1,C1..."
						}`
						actionObject := Action{player.userName, player.id, placeShips, errorBody, ""}
						player.Write(&actionObject)
						break
					}
					if len(coordinatesArr) == int(shipSize) {
						for _, coordinate := range coordinatesArr {
							coordinate = strings.TrimSpace(strings.ToUpper(coordinate))
							if coordinate == "" {
								errorBody := `{
									"error": "` + key + ` one or more coordinate is empty"
								}`
								actionObject := Action{player.userName, player.id, placeShips, errorBody, ""}
								player.Write(&actionObject)
								break
							}
							runes := []rune(coordinate)
							firstChar := int(runes[0])
							if firstChar < 65 || firstChar > 75 {
								errorBody := `{
									"error": "` + key + ` coordinate ` + coordinate + ` must be between A[1-10] and J[1-10]"
								}`
								actionObject := Action{player.userName, player.id, placeShips, errorBody, ""}
								player.Write(&actionObject)
								break
							}
							if player.grid[coordinate] != nil && player.grid[coordinate].ship == nil {
								player.grid[coordinate].ship = player.ships[shipType]
							} else {
								errorBody := `{
									"error": "` + key + ` coordinate ` + coordinate + ` already has a ship present or is an invalid coordinate"
								}`
								actionObject := Action{player.userName, player.id, placeShips, errorBody, ""}
								player.Write(&actionObject)
								break
							}
						}
					} else {
						errorBody := `{
							"error": "` + key + ` requires ` + fmt.Sprint(shipSize) + ` coordinates"
						}`
						actionObject := Action{player.userName, player.id, placeShips, errorBody, ""}
						player.Write(&actionObject)
						break
					}
				}

				game := server.findGameByPlayer(player)
				if game != nil {
					if game.player2.state == readyToStart || game.player1.state == readyToStart {
						game.player1.state = inGame
						game.player2.state = inGame
						game.playerTurn = game.player1
						actionObject := Action{game.player1.userName, game.player1.id, turn, "", ""}
						game.player1.Write(&actionObject)
						actionObject2 := Action{game.player2.userName, game.player2.id, waitForTurn, "", ""}
						game.player2.Write(&actionObject2)
					}
				}
				player.state = readyToStart
				// actionObject := Action{player.userName, player.id, placeShips, "", ""}
				// player.Write(&actionObject)
			}

		case err := <-server.errorChannel:
			log.Println("Error : ", err)
		case <-server.doneCh:
			return
		}
	}

}

func (server *Server) handleActions(responseWriter http.ResponseWriter, request *http.Request) {
	log.Println("Handling battleship request ")
	var actionObject Action
	conn, _ := upgrader.Upgrade(responseWriter, request, nil)
	//msgType, msg, err := conn.ReadMessage()
	err := conn.ReadJSON(&actionObject)
	log.Println("Action retireved from new client", &actionObject)

	if err != nil {
		log.Println("Error while reading JSON from websocket ", err.Error())
	}

	if actionObject.ActionType == newPlayer {

		player := NewPlayer(conn, server, actionObject.UserName)

		log.Println("going to add player", player)
		server.AddPlayer(player)

		log.Println("player added successfully")

		//server.ProcessNewAction(&actionObject)
		player.Listen()
	} else if actionObject.ActionType == processMessage {
		server.ProcessNewAction(&actionObject)
	}

}

func (server *Server) handleGetAllMessages(responseWriter http.ResponseWriter, request *http.Request) {
	println("handleGetAllMessages")
	json.NewEncoder(responseWriter).Encode(server)
}

func sortCoordinates(coordinates []string) []string {

	regAlphaStr := "[^a-zA-Z]+"
	regNumStr := "[^0-9]+"
	regAlpha, _ := regexp.Compile(regAlphaStr)
	regNum, _ := regexp.Compile(regNumStr)

	sort.Slice(coordinates, func(i, j int) bool {

		firstStr := regAlpha.ReplaceAllString(coordinates[i], "")
		secondStr := regAlpha.ReplaceAllString(coordinates[j], "")
		if firstStr == secondStr {
			aN, _ := strconv.ParseInt(regNum.ReplaceAllString(coordinates[i], ""), 10, 0)
			bN, _ := strconv.ParseInt(regNum.ReplaceAllString(coordinates[j], ""), 10, 0)
			return aN == bN || aN < bN
		} else {
			return firstStr < secondStr
		}
	})

	return coordinates
}

func sumCoordinate(array []rune) int {
	result := 0
	for _, v := range array {
		result += int(v)
	}
	return result
}

func isCoordinatesInOrder(coordinates []string) bool {
	prevRuneSum := 0
	for _, value := range coordinates {
		runes := []rune(strings.TrimSpace(strings.ToUpper(value)))
		runeSum := sumCoordinate(runes)
		if prevRuneSum != 0 {
			if (prevRuneSum+1) != runeSum && (prevRuneSum+40) != runeSum {
				return false
			}
		}
		prevRuneSum = runeSum
	}
	return true
}

func (server *Server) findGameByPlayer(player *Player) *Game {
	for _, game := range server.Games {
		if game.player1 == player || game.player2 == player {
			return game
		}
	}
	return nil
}
