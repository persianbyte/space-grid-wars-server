package core

import (
	"fmt"
	"log"
	"strconv"

	"github.com/gorilla/websocket"
)

type playerState int

const (
	available playerState = iota
	lfgQueue
	waitingForgame
	readyForGame
	readyToStart
	inGame
)

const channelBufSize = 100

var maxID int = 0

type Player struct {
	id             int
	conn           *websocket.Conn
	server         *Server
	outgoingAction chan *Action
	state          playerState
	doneCh         chan bool
	userName       string
	ships          map[ShipType]*Ship
	grid           map[string]*Cell
	trackingGrid   map[string]*Cell
}

func NewPlayer(conn *websocket.Conn, server *Server, userName string) *Player {
	if conn == nil {
		panic("connection cannot be nil")
	}
	if server == nil {
		panic(" Server cannot be nil")
	}

	maxID++

	carrier := &Ship{shipType: Carrier, size: CarrierSize, hits: 0}
	battleship := &Ship{shipType: Carrier, size: CarrierSize, hits: 0}
	crusier := &Ship{shipType: Carrier, size: CarrierSize, hits: 0}
	submarine := &Ship{shipType: Carrier, size: CarrierSize, hits: 0}
	destroyer := &Ship{shipType: Carrier, size: CarrierSize, hits: 0}

	ships := make(map[ShipType]*Ship)

	ships[Carrier] = carrier
	ships[Battleship] = battleship
	ships[Cruiser] = crusier
	ships[Submarine] = submarine
	ships[Destroyer] = destroyer

	grid := make(map[string]*Cell)

	trackingGrid := make(map[string]*Cell)

	for x := 65; x < 75; x++ {
		character := string(x)
		for y := 1; y < 11; y++ {
			cell := Cell{hit: false, miss: false}
			grid[character+strconv.Itoa(y)] = &cell
			trackingGrid[character+strconv.Itoa(y)] = &cell
		}
	}

	ch := make(chan *Action, channelBufSize)
	doneCh := make(chan bool)
	log.Println("Done creating new Player")
	return &Player{maxID, conn, server, ch, available, doneCh, userName, ships, grid, trackingGrid}
}

func (player *Player) Conn() *websocket.Conn {
	return player.conn
}

func (player *Player) Write(action *Action) {
	select {
	case player.outgoingAction <- action:
	default:
		player.server.RemovePlayer(player)
		err := fmt.Errorf("Player %d is disconnected.", player.id)
		player.server.Err(err)

	}
}

func (player *Player) Done() {
	player.doneCh <- true
}

func (player *Player) Listen() {
	go player.listenWrite()
	player.listenRead()
}

func (player *Player) listenWrite() {
	log.Println("Listening to write to client")

	for {
		select {

		//send message to player
		case action := <-player.outgoingAction:
			//  log.Println("send in listenWrite for player :",player.id, msg)
			player.conn.WriteJSON(&action)

			// receive done request
		case <-player.doneCh:
			log.Println("Done Channel for player:")
			player.server.RemovePlayer(player)
			player.doneCh <- true
			return

		}
	}
}

func (player *Player) listenRead() {
	//log.Println("Listening to Read to client")
	for {
		select {
		//receive Done request
		case <-player.doneCh:
			player.server.RemovePlayer(player)
			player.doneCh <- true
			return

		// read data from websocket connection
		default:
			var actionObj Action
			err := player.conn.ReadJSON(&actionObj)

			if err != nil {
				player.doneCh <- true
				log.Println("Error while reading JSON from websocket ", err.Error())
				player.server.Err(err)
			} else {
				println("listen ProcessNewIncomingMessage")
				player.server.ProcessNewAction(&actionObj)
			}

		}
	}
}
