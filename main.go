package main

import (
	"log"
	"net/http"
	"spacegridwars/core"
)

func main() {
	// websocket server
	server := core.NewServer()
	go server.Listen()

	http.HandleFunc("/messages", handleSite)
	http.HandleFunc("/", handleSite)

	// Start the server on localhost port 8000 and log any errors
	log.Println("http server started on :8000")
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func handleSite(responseWriter http.ResponseWriter, request *http.Request) {
	http.ServeFile(responseWriter, request, "index.html")
}
